+++

+++
## Bienvenue

Voici mon blog de test pour me faire la main sur Hugo. C'est tordu quand même. Mais Forestry aide vraiment beaucoup!

Allé Zou, on essaye une image? ![Sea boh non ?](/uploads/9e249eb1e5750e7eb2156867b253bfff.jpg "Sea")

Bon ok... J'ai laissé le texte d'origine:

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and can be built in under 1 minute.
Literally. It uses the `beautifulhugo` theme which supports content on your front page.
Edit `/content/_index.md` to change what appears here. Delete `/content/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.